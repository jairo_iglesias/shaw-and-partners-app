import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import { Alert, Button, Table, Modal, ModalHeader, ModalBody, ModalFooter, Progress } from 'reactstrap';

class App extends Component {

  rootUrl = 'https://shawand-partners-server.herokuapp.com'

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      users: [],
      prevLink: '',
      nextLink: '',
      showModal: false,
      backdrop: true,
      error: false,
      loading: false,
      selected: {}
    };

    this.onNext = this.onNext.bind(this)
    this.onPrev = this.onPrev.bind(this)
    this.onRowClick = this.onRowClick.bind(this)
    this.toggle = this.toggle.bind(this)
    this.handleInterceptor = this.handleInterceptor.bind(this)
  }

  styles = {
    table: {
      height: '510px', 
      overflow: 'auto',
      marginBottom: '20px'
    },
    pageStyle: {
      fontSize: '20px',
      width: '10%',
      listStyle: 'none',
      border: '1px solid',
      borderRadius: '3px',
      padding: '5px 5px'
    },
    tableRow: {
      cursor: 'pointer'
    },
  }

  handleInterceptor(){

    // Interceptor to show alert on error request limit in github API !!!

    axios.interceptors.request.use((config) => {
      
      this.setState({loading: true})

      return config;
    }, function (error) {
      return Promise.reject(error);
    });

    axios.interceptors.response.use((response) => {

      this.setState({loading: false})
      
      if(response.data.message !== undefined){
        console.log('Error GitHub API')
        this.setState({error: true})
        return false
      }
      else{
        this.setState({error: false})
        return response;
      }
      
    }, (error) => {
    
      return Promise.reject(error);

    })

  }

  componentDidMount(){

    this.handleInterceptor()

    let url = this.rootUrl + '/api/users?since=0'

    axios.get(url).then(res => {
      
      let link = res.headers['link']
      link = link.split(';')[0].replace('<', '').replace('>', '')

      this.setState({ 
        users: res.data,
        nextLink: link
      })

    })

  }

  onNext(){

    let url = this.rootUrl + this.state.nextLink

    axios.get(url).then(res => {
        
      let link = res.headers['link']
      link = link.split(';')[0].replace('<', '').replace('>', '')

      this.setState({ 
        users: this.state.users.concat(res.data),
        nextLink: link,
        prevLink: this.state.nextLink
      })

      console.log(this.state.nextLink)
      console.log(this.state.prevLink)

    })
  }

  onPrev(){

    let url = this.rootUrl + this.state.prevLink

    axios.get(url).then(res => {
        
      let link = res.headers['link']
      link = link.split(';')[0].replace('<', '').replace('>', '')

      this.setState({ 
        users: res.data,
        nextLink: link,
        prevLink: this.state.prevLink
      })

      // console.log(this.state.nextLink)
      // console.log(this.state.prevLink)

    })
  }

  onRowClick(id){

    let filter = this.state.users.filter((user) => {
      return user.id === id
    })

    let user = filter[0]

    let urlDetails = `${this.rootUrl}/api/users/${user.login}/details`

    axios.get(urlDetails).then(res => {

      user.details = res.data

      let urlRepos = `${this.rootUrl}/api/users/${user.login}/repos`

      axios.get(urlRepos).then(res => {
        
        user.repos = res.data

        this.setState({
          selected: user
        })

        this.toggle()

      })

    })

  }

  toggle(){
    this.setState({ showModal: !this.state.showModal });
  }

  render() {

    return (
      <div className="App container">

       <Alert style={{ display: this.state.error ? 'block' : 'none' }} color="danger">
       Request Limit Exceeded in GitHub API - Try again in 1 hour
       </Alert>


        <header className="App-header">
          <h1 className="App-title">GitHub User List</h1>
        </header> 
        
        <br/ >

        <div style={{height:'15px'}}>
          <div style={{display: this.state.loading ? 'block' : 'none'}}>
            <Progress animated value="100" />
          </div>
        </div>

        <br />

        {/* ### TABELA FUNCIONANDO ### */}
        <div style={this.styles.table}>
          <Table hover bordered size="sm">
          <thead>
            <tr>
              <th>ID</th>
              <th>Login</th>
            </tr>
          </thead>
          <tbody>
            {this.state.users.map((user, index) => 
              {
                return <tr style={this.styles.tableRow} key={index} onClick={() => this.onRowClick(user.id)} data-value="test2">
                  <td>{user.id}</td>
                  <td>{user.login}</td>
                </tr>
              })
            }
          </tbody>
          
          </Table>

        </div>
        
        <Button color="primary" size="lg" block onClick={this.onNext}>Load more items</Button>

        <Modal size="lg" isOpen={this.state.showModal} toggle={this.toggle} autoFocus={false} >
          <ModalHeader toggle={this.toggle}>User Details</ModalHeader>
          <ModalBody >

            <div>
              <p>
                <strong>ID: </strong>
                {this.state.selected.id}
              </p>
              <p>
                <strong>Login: </strong>
                {this.state.selected.login}
              </p>
              <p>
                <strong>Profile URL: </strong>
                {this.state.selected.details === undefined ? '' : this.state.selected.details.url}
              </p>
              <p>
                <strong>Created At: </strong>
                {this.state.selected.details === undefined ? '' : this.state.selected.details.created_at}
              </p>

              <hr />
              <p><strong>Repositories</strong></p>

              <Table striped size="sm">

              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Repository URL</th>
                </tr>
              </thead>
              <tbody>
              {
                
                (this.state.selected.repos === undefined ? [] : this.state.selected.repos)  
                .map((repoItem, repoIndex) => 
                  {
                    return <tr key={repoIndex}>
                      <td>{repoItem.id}</td>
                      <td>{repoItem.name}</td>
                      <td><a href={repoItem.html_url}>{repoItem.html_url}</a></td>
                    </tr>
                  })
                }
              </tbody>
              </Table> 

            </div>
            

          </ModalBody>

           <ModalFooter>
            <Button onClick={this.toggle}>Close</Button>
          </ModalFooter>

        </Modal>
        

      </div>
    );
  }
}

export default App;
